import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSuggestionComponent } from './hero-suggestion.component';

describe('HeroSuggestionComponent', () => {
  let component: HeroSuggestionComponent;
  let fixture: ComponentFixture<HeroSuggestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSuggestionComponent ]
    })
    .compileComponents();
  }));


  it('should create the HeroSuggestionComponent', async(() => {
    const fixture = TestBed.createComponent(HeroSuggestionComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  it('should contain h5.card-title tag', async(() => {
    const fixture = TestBed.createComponent(HeroSuggestionComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h5.card-title')).toBeTruthy();
  }));

  it('should contain .card-img-top image', async(() => {
    const fixture = TestBed.createComponent(HeroSuggestionComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.card-img-top')).toBeTruthy();
  }));

});
