import { Component, OnInit, Input } from '@angular/core';
import {Hero} from "../hero";

@Component({
  selector: 'app-hero-suggestion',
  templateUrl: './hero-suggestion.component.html',
  styleUrls: ['./hero-suggestion.component.scss']
})
export class HeroSuggestionComponent implements OnInit {

  @Input() hero: Hero;

  constructor() {}

  ngOnInit() {
  }

}
