import {Component, OnInit, Inject} from '@angular/core';
import { ApiService } from "./api.service";
import {Hero, HeroImage} from "./hero";
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Browse Marvel Heroes!';
  heroes: Array<Hero> = [];
  selected_hero: any;

  constructor(private apiService: ApiService, @Inject(DOCUMENT) private document: Document) {}

  heroName: string = '';

  onSubmit = () => {
    this.apiService.getSuggestions(this.heroName).subscribe((data) => {

      this.heroes = [];
      this.selected_hero = null;
      this.initializeHeroesList(data);

    });

    this.document.body.scrollTop = 0;

  };

  selectHero = (index: number) => {
    this.apiService.getHero(this.heroes[index].name).subscribe((data) => {
      this.initializeSelectedHero(data);
    });

    this.document.body.scrollTop = 0;
  };

  initializeSelectedHero = (data: Object) => {

    let hasHeroes: boolean = data.hasOwnProperty('data') && data['data'].hasOwnProperty('results');
    let heroesArray: Array<any> = hasHeroes ? data['data']['results'] : [];

    if(heroesArray.length == 1) this.selected_hero = heroesArray[0];

  };

  initializeHeroesList = (data: Object) => {

    let hasHeroes: boolean = data.hasOwnProperty('data') && data['data'].hasOwnProperty('results');
    let heroesArray: Array<any> = hasHeroes ? data['data']['results'] : [];

    for (let heroData of heroesArray) {

      let hero: Hero = new Hero();

      const isValid = this.validateHeroData(heroData);

      if(isValid.name) hero.setName(heroData.name);
      if(isValid.description) hero.setDescription(heroData.description);
      if(isValid.image) hero.setImage(heroData.thumbnail);

      this.heroes.push(hero);

    }

  };

  validateHeroData = (data: Object) => {

    const validName: boolean =
        data.hasOwnProperty('name') &&
        ((typeof data['name'] == 'string' || data['name'] instanceof String));

    const validDescription: boolean =
        data.hasOwnProperty('description') &&
        ((typeof data['description'] == 'string' || data['description'] instanceof String));

    const validImage: boolean =
        data.hasOwnProperty('thumbnail') &&
        data['thumbnail'].hasOwnProperty('path') &&
        data['thumbnail'].hasOwnProperty('extension');

    return {
      name: validName,
      description: validDescription,
      image: validImage
    }

  }

}
