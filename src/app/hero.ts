export class Hero {

    name: string;
    description: string;
    thumbnail: string;

    constructor() {}

    public setName = (name: string) => this.name = name;
    public setDescription = (description: string) => this.description = description;
    public setImage = (image: HeroImage) => this.thumbnail = image.path+'.'+image.extension;

}

export interface HeroImage {
    path: string,
    extension: string
}
