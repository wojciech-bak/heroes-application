import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Md5} from "ts-md5";

@Injectable()
export class ApiService {

  md5 = new Md5();
  hash = null;
  ts = null;

  // Marvel's API
  API_URL  =  'https://gateway.marvel.com/v1/public';
  // Marvel's API public key
  PUBLIC_KEY = 'a003ef0b4db5123a3c3b1d1312dd154b';
  PRIVATE_KEY = 'f06441244fd2b2e20c569d6bc54080b4c96c7a0c';

  constructor(private  httpClient:  HttpClient) {

  }

  getComic(id: number) {
    return this.request(`characters/${id}/comics`,{
      orderBy: 'onsaleDate'
    });
  }

  getHero(name) {
    return this.request('characters', {
      name: name
    });
  }

  getSuggestions(name) {
    return this.request('characters', {
      orderBy: 'name',
      nameStartsWith: name
    });
  }

  request(url, params) {

    // generate session tokens for Marvel's API
    if(!this.sessionStarted()){
      this.generateTimeStamp();
      this.generateHash();
    }

    // send request
    return this.httpClient.get(`${this.API_URL}/${url}`, {
      params: {
        ...params,
        apikey: this.PUBLIC_KEY,
        hash: this.hash,
        ts: this.ts
      }
    });

  }

  private sessionStarted = () => this.ts && this.hash;

  private generateHash = () => {
    this.hash = this.md5.appendStr(this.ts + this.PRIVATE_KEY + this.PUBLIC_KEY).end();
  };

  private generateTimeStamp = () => {
    this.ts = new Date().getTime();
  }

}
