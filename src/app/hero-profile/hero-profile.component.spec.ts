import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroProfileComponent } from './hero-profile.component';
import {ApiService} from "../api.service";
import {HttpClientModule} from "@angular/common/http";

describe('HeroProfileComponent', () => {
    let component: HeroProfileComponent;
    let fixture: ComponentFixture<HeroProfileComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HeroProfileComponent ],
            providers: [ApiService],
            imports: [
                HttpClientModule,
            ],
        })
            .compileComponents();
    }));


    it('should create the HeroProfileComponent', async(() => {
        const fixture = TestBed.createComponent(HeroProfileComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    it('should contain h1 tag', async(() => {
        const fixture = TestBed.createComponent(HeroProfileComponent);
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1')).toBeTruthy();
    }));

    it('should contain p tag after h1', async(() => {
        const fixture = TestBed.createComponent(HeroProfileComponent);
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1 + p')).toBeTruthy();
    }));

});