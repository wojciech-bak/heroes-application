import {Component, OnInit, Input, SimpleChanges, SimpleChange, OnChanges} from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-hero-profile',
  templateUrl: './hero-profile.component.html',
  styleUrls: ['./hero-profile.component.scss']
})
export class HeroProfileComponent implements OnInit, OnChanges {

  @Input() hero;

  comics;
  can_load = true;

  constructor(private apiService: ApiService) { }

  public getComic = () => {
    if(this.hero.id) {
      this.apiService.getComic(this.hero.id).subscribe((data) => {
        const hasComics:boolean = data.hasOwnProperty('data') && data['data'].hasOwnProperty('results');
        this.comics = hasComics ? data['data']['results'] : [];
        this.can_load = false;
      });
    }
  };

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    this.comics = [];
    this.can_load = true;
  }

}
