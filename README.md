# HeroesApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4 and uses [data provided by Marvel. © 2018](http://marvel.com).

## Quick start

Run `git clone git@gitlab.com:wojciech-bak/heroes-application.git heroes-app` to download project and enter the directory `heroes-app` by running `cd heroes-app`. 

Run `npm install` to load all packages needed to build the Angular application.

Run `npm start` to launch the application in development mode. 

Congratulations! You can browse Marvel heroes in your browser at `localhost:4200`.

## Superheroes data from Marvel

The _ApiService_ establishes connection with Marvel's API by generating _hash_ using `Md5` class and sending request. The solution is not secure enough to use it in production environment, because server name `localhost` has been declared as authorized domain in Marvel's account.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` or `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
